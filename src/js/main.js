$(document).ready(function() {
  $.ajax({
    url: 'http://localhost:3000/person',
    async: true,
    success: function(result) {
      // name
      $('.introduce').html(
        '<h1>HELLO,<br/>MY NAME IS ' +
          result.name +
          ' 24YO AND THIS IS MY RESUME/CV </h1>'
      );
      // description
      $('.description').html(result.description);
      // education
      var str = '<ul>';
      $.each(result.educations, function(i, item) {
        str += '<li>' + item.year + item.title + item.description + '</li>';
      });
      str += '</ul>';
      $('.edu-content').append(str);
    }
  });
});
